package com.company;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import static com.company.DatabaseHelper.insertEncarrec;
import static com.company.DatabaseHelper.openDBConnection;

public class Erp extends JFrame{
    DefaultComboBoxModel myComboBoxModel;
    JComboBox clientsComboBox;
    JComboBox productsComboBox;
    JSpinner cantidadSpinner;
    JSpinner selectEntitySpinner;
    JButton enviaEncarrecButton;
    static DefaultTableModel encarrecsTableModel;
    static JTable encarrecsTable;
    public Erp() throws SQLException, ClassNotFoundException {
        JTabbedPane myTabbedPane=new JTabbedPane();
        //FER ENCARREC PANELL
        ompleComboBox();
        JPanel afegirEncarrecPanel=new JPanel();
        JLabel afegirClientLabel = new JLabel("Tria el client");
        JLabel afegirProducteLabel = new JLabel("Afegeix els productes i quantitat");
        JLabel triaQuantitatLabel = new JLabel("Tria la quantitat");
        afegirEncarrecPanel.add(afegirClientLabel);
        afegirEncarrecPanel.add(clientsComboBox);
        JPanel addProducts = new JPanel();
        afegirEncarrecPanel.add(afegirProducteLabel);
        afegirEncarrecPanel.add(productsComboBox);
        afegirEncarrecPanel.add(triaQuantitatLabel);
        cantidadSpinner = new JSpinner();
        afegirEncarrecPanel.add(cantidadSpinner);
        enviaEncarrecButton = new JButton("Fes Encarrec");
        afegirEncarrecPanel.add(enviaEncarrecButton);
        afegirEncarrecPanel.setLayout(new GridLayout(20,1));
        //Listener buto fer encarrec
        enviaEncarrecButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                int id = 10;
                String status = "pendent";
//                insertEncarrec();
            }
        });
        //MOSTRAR ENCARRECS PANEL
        JPanel mostrarEncarrecsPanel = new JPanel();
        actualitzaTableModelEncarrecs();
        mostrarEncarrecsPanel.setLayout(new BoxLayout(mostrarEncarrecsPanel, BoxLayout.Y_AXIS));
        JScrollPane mostrarEncarrecScrollPane = new JScrollPane(encarrecsTable);
        JLabel mostrarEncarrecsLabel = new JLabel("Llistat d'encarrecs");
        mostrarEncarrecsLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        mostrarEncarrecsPanel.add(mostrarEncarrecsLabel);
        mostrarEncarrecsPanel.add(mostrarEncarrecScrollPane);
        //PANELL CRUD
        JPanel crudPanel = new JPanel();
        crudPanel.setLayout(new BoxLayout(crudPanel, BoxLayout.Y_AXIS));
        crudPanel.add(actionToolbarCrud());
        crudPanel.add(crudActionPanel());

        //Productes per pais
        JPanel paisPanel = new JPanel();
        myTabbedPane.addTab("Afegir encarrec", afegirEncarrecPanel);
        myTabbedPane.addTab("Mostrar encarrec", mostrarEncarrecsPanel);
        myTabbedPane.addTab("Full CRUD", crudPanel);
        myTabbedPane.addTab("Productes per pais", paisPanel);
        add(myTabbedPane);
        setPreferredSize(new Dimension(1000, 1000));
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }
    private void ompleComboBox() throws SQLException, ClassNotFoundException {
        myComboBoxModel = new DefaultComboBoxModel();
        clientsComboBox = new JComboBox();
        productsComboBox = new JComboBox();
        try {
            Connection connexio = openDBConnection();
            Statement pregunta = connexio.createStatement();
            ResultSet resposta = pregunta.executeQuery("select nom from clients");
            while (resposta.next()) {
                String nom = resposta.getString("nom");
                clientsComboBox.addItem(nom);
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        try {
            Connection connexio = openDBConnection();
            Statement pregunta = connexio.createStatement();
            ResultSet resposta = pregunta.executeQuery("select nom from productes");
            while (resposta.next()) {
                String nom = resposta.getString("nom");
                productsComboBox.addItem(nom);
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
    private void actualitzaTableModelEncarrecs() throws SQLException, ClassNotFoundException {
        String[] nomColumnes = {"ID Encarrec","ID Client"};
        this.encarrecsTableModel = new DefaultTableModel(null,nomColumnes);
        try {
            Connection connexio = openDBConnection();
            Statement pregunta = connexio.createStatement();
            ResultSet resposta = pregunta.executeQuery("select * from encarrecs");
            while (resposta.next()) {
                int id = resposta.getInt("id_encarrec");
                String status = resposta.getString("status");
                int idclient = resposta.getInt("idclient");
                String[] tableRow = {Integer.toString(id), Integer.toString(idclient)};
                this.encarrecsTableModel.addRow(tableRow);
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        encarrecsTable = new JTable();
        this.encarrecsTable.setModel(encarrecsTableModel);
    }
    private JToolBar actionToolbarCrud(){
        JButton butoAfegir = new JButton();
        JButton butoDelete = new JButton();
        JToolBar categoryButtons = new JToolBar();
        ImageIcon icona;
        icona = new ImageIcon("add_button.png");
        butoAfegir = new JButton("Afegir",icona);
        butoAfegir.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    //addEntryToDatabase();
                }catch (Exception ex){
                    System.out.println("Ha fallat la insercio: "+ex.getMessage());
                }
            }
        });
        categoryButtons.add(butoAfegir);
        icona = new ImageIcon("delete_button.png");
        butoDelete = new JButton("Eliminar",icona);
        butoDelete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    //deleteProductById(selectedRow());
                    //actualitzaTableModelClientes();
                } catch (Exception e) {
                    e.getMessage();
                }
            }
        });
        categoryButtons.add(butoDelete);
        return categoryButtons;
    }
    public JPanel crudActionPanel(){
        JPanel crudActionPanel = new JPanel();
        JTable clientesTable = new JTable();
        JComboBox selectEntityCombobox = new JComboBox(new String[]{"Clients","Encarrecs","Paisos","Productes"});
        selectEntityCombobox.addActionListener (new ActionListener () {
            public void actionPerformed(ActionEvent e) {
                System.out.println(selectEntityCombobox.getSelectedItem());
                crudActionPanel.removeAll();
                try {
                    actualitzaTableModelEncarrecs();
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                } catch (ClassNotFoundException classNotFoundException) {
                    classNotFoundException.printStackTrace();
                }
                clientesTable.setModel(encarrecsTableModel);
                crudActionPanel.add(clientesTable);
            }
        });
        crudActionPanel.add(selectEntityCombobox);
//        clientesTable = new JTable();
//        crudActionPanel.add(clientesTable);
        return crudActionPanel;
    }
}
