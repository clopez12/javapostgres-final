CREATE TABLE clients (
    id INTEGER NOT NULL PRIMARY KEY ,
    nom varchar(50),
    email varchar(50),
    address varchar(100)
);
CREATE TABLE paisos (
    id INTEGER NOT NULL,
    nom varchar(50)
);
CREATE TABLE productes (
    id INTEGER NOT NULL,
    nom varchar(50),
    preu varchar(50),
	codi integer,
    idpais integer
	CONSTRAINT fk_idpais
		REFERENCES paisos(id)
);
CREATE TABLE encarrecs (
    id INTEGER NOT NULL PRIMARY KEY,
    status boolean,
    idclient integer
	CONSTRAINT fk_idclient
		REFERENCES clients(id)
);
INSERT INTO encarrecs VALUES(1,'servit',1);
INSERT INTO encarrecs VALUES(2,'servit',1);
INSERT INTO encarrecs VALUES(3,'servit',1);

-- https://drive.google.com/file/d/1w-O1Lu1oTa9kJUKun0HxsdyA6hcXRJt_/view?usp=sharing

