package com.company;

import java.sql.*;

import static java.sql.DriverManager.getConnection;
public class DatabaseHelper {
    private static String urlDades = "jdbc:postgresql://postgresql-carlos-lopez-7e4.alwaysdata.net/carlos-lopez-7e4_erp";
    private static String usuari = "carlos-lopez-7e4_m3_root";
    private static String clau = "`X@k7#,SYBX-;%<F";
    private static Connection connexio;

    public static Connection openDBConnection() throws SQLException, ClassNotFoundException {
        Class.forName("org.postgresql.Driver");
        connexio = DriverManager.getConnection(urlDades, usuari, clau);
        return connexio;
    }
    public static void insertEncarrec(int id, String status, int idclient) throws SQLException {
        connexio = getConnection(urlDades, usuari, clau);
        Statement pregunta = connexio.createStatement();
        try {
            PreparedStatement consulta = connexio.prepareStatement("INSERT INTO productes VALUES(DEFAULT,?,?,?);");
            consulta.setInt(1, id);
            consulta.setString(2, status);
            consulta.setInt(3, idclient);
            consulta.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
        pregunta.close();
        connexio.close();
    }

    public static void insertProduct(String nom, String marca, int preu, int pvp, String img) throws SQLException {
        connexio = getConnection(urlDades, usuari, clau);
        Statement pregunta = connexio.createStatement();
        try {
            PreparedStatement consulta = connexio.prepareStatement("INSERT INTO productes VALUES(DEFAULT,?,?,?,?,?);");
            consulta.setString(1, nom);
            consulta.setString(2, marca);
            consulta.setInt(3, preu);
            consulta.setInt(4, pvp);
            consulta.setString(5, img);
            consulta.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
        pregunta.close();
        connexio.close();
    }
    public static void deleteProductById(int id) throws SQLException, ClassNotFoundException {
        connexio = openDBConnection();
        Statement pregunta = connexio.createStatement();
        try {
            ResultSet resposta = pregunta.executeQuery("DELETE from productes where id_producte =" + id + ";");
        } catch (Exception e) {
            System.out.println(e);
        }
        connexio.close();
        pregunta.close();
    }
    public static void updateProductById(int id, String nom, String marca, int preu, int pvp, String img) throws SQLException, ClassNotFoundException {
        connexio = openDBConnection();
        try {
            PreparedStatement consulta = connexio.prepareStatement("UPDATE productes SET nom='"+nom+"',marca='"+marca+"',preu="+preu+",pvp="+pvp+",img='"+img+"' where id_producte="+id+";");
            consulta.executeUpdate();
            consulta.close();
        } catch (Exception e) {
            System.out.println("Ha fallat l'update per id");
            System.out.println(e);
        }
        connexio.close();
    }
    public static void updateProductByName(String nom, int id) throws SQLException, ClassNotFoundException {
        connexio = openDBConnection();
        try {
            PreparedStatement consulta = connexio.prepareStatement("UPDATE productes SET id_producte="+id+" where nom='"+nom+"';");
            consulta.executeUpdate();
            consulta.close();
        } catch (Exception e) {
            System.out.println("Ha fallat l'update per nom");
            System.out.println(e);
        }
        connexio.close();
    }
}



